---
header:
    type: video
    src: /hintergrund.webm
---

# Kontakt

## Immanuel-Kant-Gymnasium Pirmasens

Wörthstr. 30 – 66953 Pirmasens

Tel. 06331 – 24040

Fax 06331 – 240423

[kant.gym@pirmasens.de](mailto:kant.gym@pirmasens.de)

https://cloud.ikgp.de
